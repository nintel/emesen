<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"?>
<html>

<head>
	<meta http-equiv="pics-label" content="(pics-1.1 &quot;http://www.icra.org/ratingsv02.html&quot; comment &quot;Single file v2.0&quot; l gen true for &quot;http://www.msn.com&quot;  r (nz 1 vz 1 lz 1 oz 1 cz 1) &quot;http://www.rsac.org/ratingsv01.html&quot; l gen true for &quot;http://www.msn.com&quot;  r (n 0 s 0 v 0 l 0))"
	/>
	<link rel="SHORTCUT ICON" href="http://web.archive.org/web/20040221050620im_/http://a.sc.msn.com/global/c/shs/favicon.ico"
	/>
	<!--<link rel="stylesheet" type="text/css" href="http://web.archive.org/web/20040221050620cs_/http://a.sc.msn.com/css/home/css-font.css?v=1"
	/>
	<link rel="stylesheet" type="text/css" href="http://web.archive.org/web/20040221050620cs_/http://a.sc.msn.com/css/home/home-std.css?v=2"
	/>-->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<title>
  	<?php bloginfo('name'); ?>
  </title>
  <script>
	  Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}</script>
</head>

<body>
 	<?php wp_head(); ?>
	<!-- WWW29 --><img width="0" height="0" src="#"
	 alt="" border="0" />
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr valign="top">
			<td valign="bottom" class="sfb"><a href="<?php bloginfo('url'); ?>"><?php
// Display the Custom Logo
the_custom_logo();

// Change to true if you want to use a default logo
$defaultlogo = false;
// The url of said default logo
$defaultlogourl = " ";
//                 ^ here
// If no Custom Logo, just display the site's name or default logo
if (!has_custom_logo()) {
	if ($defaultlogo == false) {
    ?>
    <h1 style="color:white;"><?php bloginfo('name'); ?></h1>
    <?php
	} else {
		?>
			<img src="<?php echo($defaultlogourl) ?>" width="118" height="35" />
		<?php
	}
}
?></a></td>
			<td width="76" class="sf"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7" width="76" height="35" alt=""
				 border="0" /></td>
			<td class="sfsb" valign="bottom">
				<ul id="topmenu">
					<?php wp_nav_menu( array( 'theme_location' => 'header_nav' ) ); ?>
				</ul>
			</td>
		</tr>
	</table>